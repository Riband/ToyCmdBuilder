# ToyCmdBuilder

ToyCmdBuilder是一个帮助无cmd经验的普通计算机用户制作的一个Cmd(Windows批处理)简易生成器,通过它,使用者可以自动化地制作一些简易的批处理文件,亦可以手动编辑

[![GitHub release](https://img.shields.io/github/release/Riband/ToyCmdBuilder.svg?style=flat-square)](https://github.com/Riband/ToyCmdBuilder/releases) [![GitHub (Pre-)Release Date](https://img.shields.io/github/release-date-pre/Riband/ToyCmdBuilder.svg?style=flat-square)](https://github.com/Riband/ToyCmdBuilder/releases)  

* GPL V3许可协议.
* 只支持Windows平台.
* 从信息技术课本学来的VB知识,只是做着玩而已

[下载](https://github.com/Riband/ToyCmdBuilder/releases)  
[主页](https://riband.github.io/ToyCmdBuilder/)  
  
**For learning and communication only, please use after volunteering all responsibility**  
**仅供学习交流!不提供任何担保,下载即表示您同意自行承担使用后果!**

**There will be no update in long time to come**  
**长时间内不会有时间更新**  
